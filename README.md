# TFG

## Commands

* `find_max_size` : finds the max width and height of all the images in a folder (and subfolders).
* `threshold`: process all the images in a folder (and sub-folders) replacing them given a threshold to eliminate noise. 
* `resize`:  changes the canvas size of all the images in a folder (and sub-folders) with a new width and height.
 
## Example

First step is to find the biggest width and height that can be found among all images.

```sh
$ tfg find_max_size /media/backup/tfg/words
Processing Files: 100% |############################################| Time: 0:00:17
Total files processed: 115320
Biggest image dimensions are: Width=1934 / Height=342
```

We found: **width=1934** and  **height=342** 

In order to work with the images first we need to replace the  traces of the handwriting to the same color (black), remove the surrounding noise and save the image in 8bit black and white format. 


```sh
$ tfg threshold /media/backup/tfg/words
Processing Files: 100% |############################################| Time: 0:01:43
Total files processed: 115318
```

Last step is to change all the images to the same size modifying the canvas arround them with a white background. The image is placed on the center of the new canvas.

```sh
$ tfg threshold /media/backup/tfg/words
$ tfg resize_images --w=2000 --h=350 --format=png /media/backup/tfg/words
Processing Files: 100% |############################################| Time: 0:11:41
Total files processed: 115317
```


## TODO

* find the intersection between the two word sets (dev and train)
* train different models:
  * Hidden Markov Models (http://scikit-learn.sourceforge.net/stable/modules/hmm.html)
  * Decision Trees (http://scikit-learn.org/stable/modules/tree.html#decision-trees)
  * Perceptron (http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Perceptron.html)

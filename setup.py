from setuptools import setup

setup(
    name="tfg",
    version='0.1',
    py_modules=['tfg'],
    install_requires=[
        'Click', 'progressbar2', 'numpy', 'scipy',
    ],
    entry_points='''
        [console_scripts]
        tfg=tfg:greet
    ''',
)
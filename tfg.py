import click

import process
from image import BaseImage, FormattedImage, DeleteImage, ThresholdImage


@click.group()
def greet():
    pass


@greet.command()
@click.option('--f', '--format', multiple=True, default=['png'], help='File formats: png, jpg, gif... default is PNG')
@click.argument('path')
def find_max_size(path, format):
    base_image = BaseImage(0, 0)
    process.directory(path, format, base_image)
    click.echo('Biggest image dimensions are: Width={} / Height={}'.format(base_image.width, base_image.height))


@greet.command()
@click.option('--f', '--format', multiple=True, default=['png'], help='File formats: png, jpg, gif... default is PNG')
@click.option('--w', '--width', default='2000', help='Image Width. Default is 2000')
@click.option('--h', '--height', default='2000', help='Image Height. Default is 2000')
@click.argument('path')
def resize(path, format, width, height):
    formatted_image = FormattedImage(int(width), int(height))
    process.directory(path, format, formatted_image)


@greet.command()
@click.option('--t', '--threshold', default='200', help='Sets the treshold of the pixel color to transform')
@click.option('--f', '--format', multiple=True, default=['png'], help='File formats: png, jpg, gif... default is PNG')
@click.argument('path')
def threshold(path, threshold, format):
    threshold_image = ThresholdImage(int(threshold))
    process.directory(path, format, threshold_image)


@greet.command()
@click.argument('path')
def delete(path):
    image = DeleteImage('-out.png')
    process.directory(path, ['png'], image)


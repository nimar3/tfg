import os

import progressbar


# loops trough a directory and process each file
def directory(root, file_formats, o):
    number_of_files = (sum([len(files) for r, d, files in os.walk(root)]))

    widgets = [
        'Processing Files: ', progressbar.Percentage(),
        ' ', progressbar.Bar(),
        ' ', progressbar.ETA(),
    ]
    bar = progressbar.ProgressBar(max_value=number_of_files, widgets=widgets, redirect_stdout=True).start()

    i = images_processed = 0

    for path, subdirs, files in os.walk(root):
        for filename in files:
            if filename.split(".")[-1] in file_formats:
                o.process(os.path.join(path, filename))
                images_processed = images_processed + 1
            i = i + 1
            bar.update(i)

    bar.finish()

    print("Total files processed: {}".format(images_processed))
import os

import numpy
from PIL import Image
from scipy.misc import imsave


class BaseImage(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def process(self, filename):

        try:
            with Image.open(filename, 'r') as image:
                w, h = image.size
                self.width = max(self.width, w)
                self.height = max(self.height, h)
        except IOError:
            print("ERROR: Found an invalid image: " + filename)
            os.remove(filename)


class ThresholdImage(object):
    def __init__(self, threshold):
        self.threshold = threshold

    def process(self, filename):

        try:
            with Image.open(filename) as image:
                image = image.convert('L')
                image = numpy.array(image)
                image = numpy.where(image > self.threshold, 1, 0)
                imsave(filename, image)
        except IOError:
            print("ERROR: Found an invalid image: " + filename)
            os.remove(filename)


class FormattedImage(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def process(self, filename):
        try:
            with Image.open(filename, 'r') as image:
                w, h = image.size
                background = Image.new('L', (self.width, self.height), 255)
                background.paste(image, (int((self.width - w) / 2), int((self.height - h) / 2)))
                background.save(filename)

        except IOError:
            print("ERROR: Found an invalid image: " + filename)
            os.remove(filename)


class DeleteImage(object):
    def __init__(self, ends):
        self.ends = ends

    def process(self, filename):
        if filename.endswith(self.ends):
            os.remove(filename)
